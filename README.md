# Entrevoisins

`Entrevoisins` est une application qui permet à des personnes d’un même quartier de se rendre des petits services : garde d’animaux, petit bricolage, troc d’objets, cours particuliers, de nombreuses options s’offrent aux utilisateurs !

## Installation
Pour récupérer l'application, vous pouvez :
- Cloner l'application via cette commande `git clone https://gitlab.com/openclassrooms_app_android/entrevoisins`
- Telécharger l'archive ci-dessus via le bouton de teléchargement

## Utilisation
Pour utiliser cette application, nous vous conseillons d'utiliser un IDE comme [Android Studio](https://developer.android.com/studio) afin de pouvoir compiler celle-ci sur un emulateur ou/et sur votre smartphone.

### Dépendances utilisées

```groovy
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'com.android.support:appcompat-v7:28.0.0'
    implementation 'com.android.support:design:28.0.0'
    implementation 'com.android.support.constraint:constraint-layout:1.1.3'
    implementation 'com.android.support:support-v4:28.0.0'
    implementation 'com.android.support:recyclerview-v7:28.0.0'

    implementation 'com.jakewharton:butterknife:9.0.0'
    annotationProcessor 'com.jakewharton:butterknife-compiler:9.0.0'

    implementation 'com.github.bumptech.glide:glide:4.9.0'
    annotationProcessor 'com.github.bumptech.glide:compiler:4.9.0'

    implementation 'org.greenrobot:eventbus:3.1.1'

    testImplementation 'junit:junit:4.12'
    testImplementation 'org.hamcrest:java-hamcrest:2.0.0.0'

    androidTestImplementation 'com.android.support.test:rules:1.0.2'
    androidTestImplementation 'com.android.support.test:runner:1.0.2'
    androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.2'
    androidTestImplementation 'com.android.support.test.espresso:espresso-contrib:3.0.2'
    androidTestImplementation "com.android.support.test.espresso:espresso-intents:3.0.2"

    implementation 'com.android.support:cardview-v7:28.0.0'
}
```

## Auteur
[Deyine Développeur Android](https://github.com/Deyine)
[Valentin Etudiant OpenClassrooms](https://openclassrooms.com/fr/members/5zbcwst2hpq8)

## Licence

```text
Copyright 2020 Entrevoisins Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this application and associated documentation files (the "Entrevoisins Application"), to deal in the Application without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Application, and to permit persons to whom the Application is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Application.

THE APPLICATION IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE APPLICATION OR THE USE OR OTHER DEALINGS IN THE APPLICATION.
```
