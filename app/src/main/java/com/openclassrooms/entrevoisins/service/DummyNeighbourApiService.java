package com.openclassrooms.entrevoisins.service;

import com.openclassrooms.entrevoisins.enums.TargetList;
import com.openclassrooms.entrevoisins.model.Neighbour;

import java.util.ArrayList;
import java.util.List;

/**
 * Dummy mock for the Api
 */
public class DummyNeighbourApiService implements NeighbourApiService {

    private List<Neighbour> neighbours = DummyNeighbourGenerator.generateNeighbours();

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Neighbour> getNeighbours(TargetList where) {
        switch(where) {
            case ALL:
                return neighbours;
            case FAVORITE:
                List<Neighbour> result = new ArrayList<>();
                for(Neighbour neighbour : neighbours) {
                    if(neighbour.isFavorite()) {
                        result.add(neighbour);
                    }
                }
                return result;
            default:
                return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteNeighbour(Neighbour neighbour) {
        neighbours.remove(neighbour);
    }

    /**
     * {@inheritDoc}
     * @param neighbour
     */
    @Override
    public void createNeighbour(Neighbour neighbour) {
        neighbours.add(neighbour);
    }
}
