package com.openclassrooms.entrevoisins.controller;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.openclassrooms.entrevoisins.R;
import com.openclassrooms.entrevoisins.di.DI;
import com.openclassrooms.entrevoisins.enums.TargetList;
import com.openclassrooms.entrevoisins.model.Neighbour;
import com.openclassrooms.entrevoisins.service.NeighbourApiService;

import java.util.List;

public class ProfileActivity extends AppCompatActivity {

    private static final String LOG = ProfileActivity.class.getSimpleName();
    private ImageButton mPreviousPage;
    private RelativeLayout mBackground;
    private TextView mNameHeader,mNameInfos,mAddress,mPhone,mWebSite,mAboutMe;
    private ImageView mBgHeader;
    private FloatingActionButton mFavorite;

    private NeighbourApiService mApiService;
    private Neighbour neighbour;
    private String textToast;
    private TargetList targetList = TargetList.FAVORITE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mApiService = DI.getNeighbourApiService();

        // GET TARGET ID
        mBgHeader = (ImageView) findViewById(R.id.bg__header);
        mBackground = (RelativeLayout) findViewById(R.id.container__header);
        mNameHeader = (TextView) findViewById(R.id.name__header);
        mNameInfos = (TextView) findViewById(R.id.name__infos);
        mAddress = (TextView) findViewById(R.id.address);
        mWebSite = (TextView) findViewById(R.id.website);
        mPhone = (TextView) findViewById(R.id.phone);
        mAboutMe = (TextView) findViewById(R.id.description);
        mPreviousPage = (ImageButton) findViewById(R.id.previous__activity);
        mFavorite = (FloatingActionButton) findViewById(R.id.favorite);

        // INTENT
        neighbour = getIntent().getParcelableExtra("NEIGHBOUR");
        Log.d(LOG, "[LOG] --> Affichage du profil '" + neighbour.getName() + "'");

        // CHECK IF THE NEIGHBOUR IS FAVORITE
        if(neighbour.isFavorite()) {
            mFavorite.setImageResource(R.drawable.ic_star_yellow_24dp);
        }

        // SET VALUES
        Glide.with(mBgHeader.getContext())
                .load(neighbour.getAvatarUrl())
                .apply(RequestOptions.centerCropTransform())
                .into(mBgHeader);
        mNameHeader.setText(neighbour.getName());
        mNameInfos.setText(neighbour.getName());
        mAddress.setText(neighbour.getAddress());
        mWebSite.setText("www.facebook.com/" + neighbour.getName().toLowerCase());
        mPhone.setText(neighbour.getPhoneNumber());
        mAboutMe.setText(neighbour.getAboutMe());

        // EVENT
        mPreviousPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickFavorite();
            }
        });
    }

    private void onClickFavorite() {
        targetList = TargetList.ALL;
        List<Neighbour> neighbours = mApiService.getNeighbours(targetList);
        int index = (int) (neighbour.getId() - 1);

        if(neighbour.isFavorite()) {
            neighbour.setFavorite(0);
            neighbours.set(index, neighbour);
            textToast = neighbour.getName() + " a été supprimé des favoris !";
            mFavorite.setImageResource(R.drawable.ic_star_white_24dp);
        } else {
            neighbour.setFavorite(1);
            neighbours.set(index, neighbour);
            textToast = neighbour.getName() + " a été ajouté aux favoris !";
            Log.d(LOG, "[LOG] --> " + textToast + " isFavorite: " + neighbour.getFavorite());
            mFavorite.setImageResource(R.drawable.ic_star_yellow_24dp);
        }
        Toast.makeText(getApplicationContext(), textToast, Toast.LENGTH_SHORT).show();
    }
}