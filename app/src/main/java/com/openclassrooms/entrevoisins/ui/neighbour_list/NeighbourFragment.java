package com.openclassrooms.entrevoisins.ui.neighbour_list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.openclassrooms.entrevoisins.R;
import com.openclassrooms.entrevoisins.controller.ProfileActivity;
import com.openclassrooms.entrevoisins.di.DI;
import com.openclassrooms.entrevoisins.enums.TargetList;
import com.openclassrooms.entrevoisins.events.DeleteNeighbourEvent;
import com.openclassrooms.entrevoisins.listeners.NeighbourListener;
import com.openclassrooms.entrevoisins.model.Neighbour;

import com.openclassrooms.entrevoisins.service.NeighbourApiService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;


public class NeighbourFragment extends Fragment implements NeighbourListener {

    private static final String LOG = NeighbourFragment.class.getSimpleName();
    private NeighbourApiService mApiService;
    private List<Neighbour> mNeighbours;
    private RecyclerView mRecyclerView;

    private static final String POSITION = "POSITION";
    private Boolean isFavoriteTab = false;
    private TargetList targetList = TargetList.ALL;

    /**
     * Create and return a new instance
     * @return @{@link NeighbourFragment}
     */
    public static NeighbourFragment newInstance(int position) {
        Log.d(LOG, "[LOG] --> Création du fragment NeighbourFragment");
        Bundle bundle = new Bundle();
        bundle.putInt(POSITION, position);
        NeighbourFragment neighbourFragment = new NeighbourFragment();
        neighbourFragment.setArguments(bundle);
        return neighbourFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            if(getArguments().getInt(POSITION) == 1) {
                isFavoriteTab = true;
                targetList = TargetList.FAVORITE;
            }
        }
        mApiService = DI.getNeighbourApiService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG, "[LOG] --> Création de la vue NeighbourFragment");
        View view;
        if(isFavoriteTab) {
            view = inflater.inflate(R.layout.fragment_favorite_neighbour_list, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment_neighbour_list, container, false);
        }
        Context context = view.getContext();
        mRecyclerView = (RecyclerView) view;
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        return view;
    }

    /**
     * Init the List of neighbours
     */
    public void initList() {
        Log.d(LOG, "[LOG] --> MAJ Liste NeighbourFragment");
        mNeighbours = mApiService.getNeighbours(targetList);
        mRecyclerView.setAdapter(new MyNeighbourRecyclerViewAdapter(mNeighbours, this, isFavoriteTab));
    }

    @Override
    public void onResume() {
        super.onResume();
        initList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    /**
     * Appel de l'activité PROFIL
     * @param neighbour
     */
    @Override
    public void onClickNeighbour(Neighbour neighbour) {
        Intent profile = new Intent(NeighbourFragment.this.getActivity(), ProfileActivity.class);
        profile.putExtra("NEIGHBOUR", neighbour);
        startActivity(profile);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Fired if the user clicks on a delete button
     * @param event
     */
    @Subscribe
    public void onDeleteNeighbour(DeleteNeighbourEvent event) {
        mApiService.deleteNeighbour(event.neighbour);
        String toastText = event.neighbour.getName() + " a été supprimé avec succès !";
        Toast.makeText(getActivity(), toastText, Toast.LENGTH_SHORT).show();
        initList();
    }


}
