package com.openclassrooms.entrevoisins.listeners;

import com.openclassrooms.entrevoisins.model.Neighbour;

public interface NeighbourListener {
    void onClickNeighbour(final Neighbour neighbour);
}
