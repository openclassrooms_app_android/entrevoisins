package com.openclassrooms.entrevoisins.service;

import android.util.Log;

import com.openclassrooms.entrevoisins.di.DI;
import com.openclassrooms.entrevoisins.enums.TargetList;
import com.openclassrooms.entrevoisins.model.Neighbour;

import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Unit test on Neighbour service
 */
@RunWith(JUnit4.class)
public class NeighbourServiceTest {

    private static final String LOG = NeighbourServiceTest.class.getSimpleName();
    private NeighbourApiService service;
    private TargetList targetList = TargetList.ALL;

    @Before
    public void setup() {
        service = DI.getNewInstanceApiService();
    }

    @Test
    public void getNeighboursWithSuccess() {
        List<Neighbour> neighbours = service.getNeighbours(targetList);
        List<Neighbour> expectedNeighbours = DummyNeighbourGenerator.DUMMY_NEIGHBOURS;
        assertThat(neighbours, IsIterableContainingInAnyOrder.containsInAnyOrder(expectedNeighbours.toArray()));
    }

    @Test
    public void deleteNeighbourWithSuccess() {
        Neighbour neighbourToDelete = service.getNeighbours(targetList).get(0);
        service.deleteNeighbour(neighbourToDelete);
        assertFalse(service.getNeighbours(targetList).contains(neighbourToDelete));
    }

    @Test
    public void deleteNeighbourFavoriteWithSuccess() {
        targetList = TargetList.ALL;
        service.getNeighbours(targetList).get(0).setFavorite(1);

        targetList = TargetList.FAVORITE;
        assertEquals(1, service.getNeighbours(targetList).size());
        assertTrue(service.getNeighbours(targetList).contains(service.getNeighbours(targetList).get(0)));

        service.getNeighbours(targetList).get(0).setFavorite(0);
        assertEquals(0, service.getNeighbours(targetList).size());
    }

    @Test
    public void addNeighbourWithSuccess() {
        int id = service.getNeighbours(targetList).size() + 1;
        final int ITEMS_COUNT = service.getNeighbours(targetList).size();
        Neighbour neighbour = new Neighbour(id, "test", "www.google.com", "1, rue du test", "0601020304", "Description", 0);

        assertEquals(ITEMS_COUNT, service.getNeighbours(targetList).size());
        service.createNeighbour(neighbour);

        Neighbour getNeighbourTest = service.getNeighbours(targetList).get(0);
        assertEquals(ITEMS_COUNT + 1, service.getNeighbours(targetList).size());
        assertTrue(service.getNeighbours(targetList).contains(neighbour));
        assertEquals(1, getNeighbourTest.getFavorite());
    }

    @Test
    public void addNeighbourFavoriteWithSuccess() {
        targetList = TargetList.FAVORITE;
        assertEquals(0, service.getNeighbours(targetList).size());

        targetList = TargetList.ALL;
        Neighbour neighbour = service.getNeighbours(targetList).get(0);
        neighbour.setFavorite(1);

        targetList = TargetList.FAVORITE;
        assertEquals(1, service.getNeighbours(targetList).size());
        assertTrue(service.getNeighbours(targetList).contains(neighbour));
        assertTrue(neighbour.isFavorite());
    }
}
